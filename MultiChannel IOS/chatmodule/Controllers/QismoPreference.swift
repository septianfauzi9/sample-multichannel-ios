//
//  QismePreference.swift
//  qisme
//
//  Created by qiscus on 12/22/16.
//  Copyright © 2016 qiscus. All rights reserved.
//

import Foundation

class QismoPreference{
    private var pref = UserDefaults.standard
    static let instance = QismoPreference()
    private init(){
    }
    
    func setRoomId(roomID: String) {
        pref.set(roomID, forKey: "roomID")
        sync()
    }
    
    func getRoomId()->String{
        return pref.string(forKey: "roomID") ?? ""
    }
    
    func clearAll(){
        pref.removeObject(forKey: "roomID")
        pref.synchronize()
    }
    
    func sync(){
        pref.synchronize()
    }
}
