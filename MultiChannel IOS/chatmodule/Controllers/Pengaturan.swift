//
//  AppDelegate.swift
//  qiscus-sdk-ios-sample-v2
//
//  Created by Rohmad Sasmito on 11/7/17.
//  Copyright © 2017 Qiscus Technology. All rights reserved.
//

import UIKit
import QiscusCore
import SwiftyJSON

class Pengaturan : NSObject {
    public var  appID = "takal-q3ucpqd1q3nrsbm"
    static let instance = Pengaturan()
    var window: UIWindow?
//
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        // Override point for customization after application launch.
//
//        QiscusCore.enableDebugPrint = true
//        QiscusCore.setup(WithAppID: appID)
//        UINavigationBar.appearance().barTintColor = UIColor.white
//        UINavigationBar.appearance().tintColor = UIColor.white
//
//        // check user is already logged in?
//        self.validateUser()
//
//        return true
//    }
//
//    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
//        return true
//    }
    
    func registerDeviceToken(){
        if let deviceToken = UserDefaults.standard.getDeviceToken(){
            QiscusCore.shared.register(deviceToken: deviceToken, onSuccess: { (response) in
                print("success register device token =\(deviceToken)")
            }) { (error) in
                print("failed register device token = \(error.message)")
            }
        }
    }
    		
    func validateUser() {
        let targetVC                    = LoginVC()
        
        let navController               = UINavigationController()
        navController.viewControllers   = [targetVC]
        
        //change navigationBar color
        UINavigationBar.appearance().barTintColor = UIColor(red: 77/255, green: 177/255, blue: 132/255, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.white
        
//        let root                            = navController
//        self.window                         = UIWindow(frame: UIScreen.main.bounds)
//        self.window!.rootViewController     = root
//        self.window!.backgroundColor        = UIColor.green
//        self.window!.makeKeyAndVisible()
    }
    
    func validateUserToken(identityToken :String,islogin:Bool){
        QiscusCore.login(withIdentityToken: identityToken, onSuccess: { (userModel) in
            QiscusCore.connect()
            self.registerDeviceToken()
            
            self.currentViewController()?.navigationController?.isNavigationBarHidden = false
            
            QiscusCore.shared.getRoom(withID: QismoPreference.instance.getRoomId(), onSuccess: { (room, comments) in
                let target = UIChatViewController()
                target.room = room
                target.islogin = islogin
                self.currentViewController()?.navigationController?.pushViewController(target, animated: true)
            }) { (error) in
                print("error getRoom =\(error.message)")
            }
        }, onError: { (error) in
            print("error login =\(error.message)")
        })
        
    }
    
    func currentViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return currentViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return currentViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return currentViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return currentViewController(base: presented)
        }
        
        return base
    }

}

extension AppDelegate : QiscusConnectionDelegate {
    func connectionState(change state: QiscusConnectionState){
        
    }
    func onConnected(){
        
    }
    func onReconnecting(){
        
    }
    func onDisconnected(withError err: QError?){
        
    }
    
}

// MARK: - Push Notification Configuration
extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var tokenString: String = ""
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        
        print("AppDelegate. didRegisterForRemoteNotificationsWithDeviceToken: \(tokenString)")
        UserDefaults.standard.setDeviceToken(value: tokenString)
        if QiscusCore.isLogined{
            QiscusCore.shared.register(deviceToken: tokenString, onSuccess: { (success) in
                print("success register device token =\(tokenString)")
            }, onError: { (error) in
                print("error register device token =\(error.message)")
            })
        }
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        print("AppDelegate. didReceive: \(notification)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("AppDelegate. didReceiveRemoteNotification: \(userInfo)")
        
        //you can custom redirect to chatRoom
        
        let userInfoJson = JSON(arrayLiteral: userInfo)[0]
        if let payload = userInfo["payload"] as? [String: Any] {
            if let payloadData = payload["payload"] {
                let jsonPayload = JSON(arrayLiteral: payload)[0]
                
                let messageID = jsonPayload["id_str"].string ?? ""
                let roomID = jsonPayload["room_id_str"].string ?? ""
                
                if !messageID.isEmpty && !roomID.isEmpty{
                    QiscusCore.shared.updateCommentReceive(roomId: roomID, lastCommentReceivedId: messageID)
                }
            }
        }
    }
}
